#!/bin/sh
# When pushing out an image to DockerHub without specifying the tag, it will look for the latest
# and the tag ID that it is associated with.
name=node-sandbox
image=amelon1/${image}
docker push $image



<todo>

  <h3 id="app-title" data-title="riot-app">{ opts.title }</h3>

  <ul>
    <li each={ items.filter(whatShow) }>
      <label class={ completed: done }>
        <input type="checkbox" checked={ done } onclick={ parent.toggle }> { title }
      </label>
    </li>
  </ul>

  <form onsubmit={ add }>
    <table>
      <tr>
        <td valign="top">
          <input data-type="input-box" ref="input" onkeyup={ edit }>
        </td>

        <td valign="top">
          <button class="add" data-type="add-item" disabled={ !text }>
            Add #{ items.filter(whatShow).length + 1 }
          </button>
        </td>
      </tr>
      <tr>
        <td valign="top">
        </td>
        <td valign="top">
          <button class="remove" data-type="delete-item" type="button" disabled={ items.filter(onlyDone).length == 0 } onclick={ removeAllDone }>
            Remove Done ({ items.filter(onlyDone).length })
          </button>
        </td>
      </tr>
  </form>

  <!-- this script tag is optional -->
  <script>
    this.items = opts.items

    edit(e) {
      this.text = e.target.value
    }

    add(e) {
      if (this.text) {
        this.items.push({ title: this.text })
        this.text = this.refs.input.value = ''
      }
      e.preventDefault()
    }

    removeAllDone(e) {
      this.items = this.items.filter(function(item) {
        return !item.done
      })
    }

    // an two example how to filter items on the list
    whatShow(item) {
      return !item.hidden
    }

    onlyDone(item) {
      return item.done
    }

    toggle(e) {
      var item = e.item
      item.done = !item.done
      return true
    }
  </script>

</todo>

### Notes
* This is a Node.js sandbox which runs a simple Todo app (borrowed from the Riot.js project - https://github.com/riot/examples/). It is designed to run inside a Docker container.

### Usage
* Clone this repo. Then build:

        % cd docker-riot-todoapp
        % ./build_docker_image.sh

* Run the app:
        % docker-compose up -d

* Verify that app is running. You should see:

        % docker-compose ps
        Name                       Command           State           Ports
        --------------------------------------------------------------------------------------
        dockerriottodoapp_sandbox_1   /code/entrypoint_app.sh   Up      0.0.0.0:3000->3000/tcp

        % docker-compose logs
        Attaching to dockerriottodoapp_sandbox_1
        sandbox_1  |
        sandbox_1  | Superstatic started.
        sandbox_1  | Visit http://0.0.0.0:3000 to view your app.

* Now point your browser to app at http://localhost:3000/.

* Shut down container when done.

        % docker-compose down

### History
* 2017-08-22
    * tag:1  - Initial checkin

